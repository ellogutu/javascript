Object.create = function (o) {
  var F = function () {};
  F.prototype = o;
  return new F();
};

var obj1 = {
  name: "Ben",
  address: {
    street: "123 Main St"
  }
};
document.writeln(obj1.address.street);


var obj2 = Object.create(obj1);
// Changes to prototype are reflected in all objects that
// link to it.
obj1.address.street = "246 Commerce";
document.writeln(obj2.address.street);

// A child may modify the value of an object in its prototype
obj2.address.street = "Changed my prototype";
document.writeln(obj1.address.street);
document.writeln('Hello, world!');
